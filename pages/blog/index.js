import Card from '../../components/layout/blog/card'
import Link from 'next/dist/client/link'
import { useRouter } from 'next/router'
import { useSession } from 'next-auth/react'
import { motion } from "framer-motion"
import {  useState } from 'react'
import BigCard from '../../components/layout/blog/bigcard'

export default function Blog(props) {
  const { data: session, status } = useSession();
  const [blog, setBlog] = useState(props.data);
  const router = useRouter();

  return (
      <div className="flex flex-col">
        <div className="flex space-x-12 place-items-center mb-8">
          <h1 className="text-3xl font-bold">
            Halaman Blog
          </h1>
          {session && session.user.image ?
          <motion.button whileHover={{ scale: 1.1 }} whileTap={{ scale: 0.9 }}  onClick={() => router.push('/blog/create')}  className="rounded-lg px-4 py-2 bg-blue-800 font-semibold outline-black">
            Create
          </motion.button> : null}
        </div>
        <div className="flex flex-wrap justify-center md:justify-between gap-4">
          {blog.length == 0 ? <p>No blogs</p>: null}
          {blog.map((i, index) => (
            index == blog.length-1 ? 
            <Link href={`/blog/${i.id}`}>
              <motion.a 
                style={{width: "100%"}}
                className="mb-8 cursor-pointer"
                initial={{ y: 30, opacity: 0 }}
                animate={{ y: 0, opacity: 1 }}
                transition={{ delay: `0.05` }}>
                <BigCard id={i.id} img={i.image} title={i.title} desc={i.desc} />
              </motion.a>
            </Link>
            : <Link href={`/blog/${i.id}`}>
              <motion.a 
              className="mb-8 cursor-pointer"
              style={{width: "30%", minWidth:"300px"}}
              initial={{ y: 30, opacity: 0 }}
              animate={{ y: 0, opacity: 1 }}
              transition={{ delay: 0.7-`0.${index}5` }}>
                <Card id={i.id} img={i.image} title={i.title} desc={i.desc} />
              </motion.a>
            </Link>)).reverse()}
        </div>
      </div>
  )
}

export async function getServerSideProps() {
  // fetch data
  const response = await fetch('https://borealis-eta.vercel.app/api/blog/fetch', {
    method: 'GET',
    headers: {
        'Content-Type': 'application/json'
    }
  });

  const datas = await response.json();

  return {
      props: {
          data: datas.data.map(data => ({
                title: data.title,
                desc: data.desc,
                image: data.image,
                comment: data.comment,
                id: data._id.toString(),
            }))
      },
  };
}
