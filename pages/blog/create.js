import Head from 'next/head'
import Image from 'next/image'
import CreateForm from '../../components/layout/blog/createForm'
import { useRouter } from 'next/router'
import { useSession } from 'next-auth/react'
import { useEffect } from 'react'

async function createBlog(title, image, desc) {
  const response = await fetch('/api/blog/create', {
      method: 'POST',
      body: JSON.stringify({title, image, desc}),
      headers: {
          'Content-Type': 'application/json'
      }
  });
  
  const data = await response.json()

  if(!response.ok) {
      return false;
  }
  return data;
}

export default function Create() {
  const router = useRouter();
  const {data: session, status} = useSession();

  useEffect(() => {
    if(session == null || !session.user.image) router.back()
  }, [])

  const styling = <style>{`
        .card {
          margin: auto;
          width: 100%;
          padding: 24px;
          box-shadow: 4px 4px 4px rgba(0, 0, 0, 0.25);
          border-radius: 8px;
          background: #292929;
        }

        form {
            margin: 32px;
        }
        input, textarea {
            background: #3C3C3C;
            margin: 12px 0px 16px 0px;
            text-indent: 4px;
            justify-self: center;
            width: 100%;
            padding: 4px 8px;
            border-radius: 4px;
        }
        textarea {
          min-height: 300px;
        }
        input:focus, textarea:focus {
            outline: transparent;
            border: 0px solid #F2F2F2;
        }
        p{
            margin: 8px 0px;
        }
        button {
            background: #1e40af;
            text-align: center;
            border-radius: 8px;
            padding: 8px 24px;
        }
    `}</style>

  async function submitHandler(props) {
    try {
      const response = await createBlog(props.title, props.image, props.desc);
      console.log(response)
      if(response.data.acknowledged) router.push('/blog')
    } catch (error) {
      console.log(error)
    }
  }
  
  return (
      <main>
        <br/>
        <div className='card'>
          <h2 className='text-[24px] font-bold text-center'>Buat blog baru</h2>
          <CreateForm onSubmit={submitHandler} />
        </div>
        {styling}
      </main>
  )
}
