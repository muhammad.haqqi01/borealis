import { useState } from 'react';
import Blog from '../../../components/layout/blog/[blogId]/Blog';
import Comment from '../../../components/layout/blog/[blogId]/Comment';
import { useEffect } from 'react';
import { useRouter } from 'next/router';
import Router from 'next/router';

async function createComment(fullname, comment, size, query) {
  const response = await fetch('/api/blog/comment', {
      method: 'POST',
      body: JSON.stringify({fullname, comment, size, query}),
      headers: {
          'Content-Type': 'application/json'
      }
  });
  return {
    response: response,
    comment: {
      id: parseInt(size),
      fullname: fullname,
      comment: comment,
    }
  };
}

async function deleteComment(index, id) {
  const response = await fetch(`/api/blog/deletecomment`, {
      method: 'POST',
      body: JSON.stringify({index, id}),
      headers: {
          'Content-Type': 'application/json'
      }
  });
  return response;
}

async function editComment(commentId, fullname, comment, id) {
  console.log(commentId, fullname, comment, id)
  const response = await fetch(`/api/blog/editcomment`, {
      method: 'POST',
      body: JSON.stringify({commentId, fullname, comment, id}),
      headers: {
          'Content-Type': 'application/json'
      }
  });
  return {
    response: response,
    comment: {
      id: parseInt(commentId),
      fullname: fullname,
      comment: comment,
    }
  };
}

export default function Id(props) {
  const router = useRouter();
  useEffect(() => {

    if(props.error != undefined){
      router.back()
    }
    else {
      setTitle(props.blogData.title)
      setImage(props.blogData.image)
      setDesc(props.blogData.desc.split('\n'))
      setComment(props.blogData.comment)
      setIsLoading(false)
    }
    
  }, [])

  const { query } = useRouter();
  const [title, setTitle] = useState();
  const [image, setImage] = useState();
  const [desc, setDesc] = useState();
  const [comment, setComment] = useState()
  const [isLoading, setIsLoading] = useState(true)

  async function submitHandler(props) {
    try {
      const response = await createComment(props.fullname, props.comment, props.id, query.blogId)
      .then((response) => {
        if(response.response.message = "Comment success"){
          let newComment = comment;
          newComment.push(response.comment)
          setComment(newComment)
        }
      })
      .then(() => {
        return {response: response, comment: comment}
      })
    } catch (error) {
      console.log(error)
    }
  }

  async function editHandler(props) {
    try {
      const response = await editComment(props.id, props.fullname, props.comment, query.blogId)
      .then((response) => {
        if(response.response.ok){
          let newComment = comment;
          newComment[props.id] = response.comment
          setComment(newComment)
        }
      })
      .then(() => {
        return {response: response, comment: comment}
      })
    } catch (error) {
      console.log(error)
    }
  }

  async function deleteHandler(index) {
    try {
      const response = await deleteComment(index, query.blogId)
      .then((response) => {
        if(response.ok){
          let newComment = comment;
          let finded = newComment.findIndex((obj => obj.id == index))
          newComment.splice(finded, 1)
          setComment(newComment)
        }
      })
      .then(() => {
        return response
      })
    } catch (error) {
      console.log(error)
    }
    
  }


  return (
      <div className='relative flex flex-col m-auto' style={{width: "80%"}}>
        {!isLoading ? <>
          <Blog title={title} image={image} desc={desc} />
        <br/>
        <hr/>
        <br/>
        <Comment comment={comment} onEdit={editHandler} onDelete={deleteHandler} onSubmit={submitHandler} />
        </> : null}
      </div>
  )
}

export async function getStaticPaths() {
  //Fetch all path available to open
  const response = await fetch('https://borealis-eta.vercel.app/api/blog/getpath', {
    method: 'GET',
    headers: {
        'Content-Type': 'application/json'
    }
  });

  const datas = await response.json()

  return {
      fallback: 'blocking',
      paths: datas.data.map(data => ({
          params: { blogId: data._id.toString() }
      }))
  }
}

export async function getStaticProps(context) {

  const blogId = context.params.blogId;

  //fetch data
  try {
    const response = await fetch(`https://borealis-eta.vercel.app/api/blog/${blogId}`, {
      method: 'GET',
      headers: {
          'Content-Type': 'application/json'
      }
    });
    const selectedBlog = await response.json();
    return {
      props: {
          blogData: {
              id: selectedBlog.data._id.toString(),
              title: selectedBlog.data.title,
              image: selectedBlog.data.image,
              desc: selectedBlog.data.desc,
              comment: selectedBlog.data.comment,
          }
      },
      revalidate: 5,
  }
  } catch (error) {
    return {
      props: {
        blogData: {},
        error: "No blog found"
      }
    }
  }
}