import { useState, useEffect } from "react";
import { useRouter } from "next/router";
import Loading from "../components/layout/Loading";
import { SessionProvider } from 'next-auth/react';
import Layout from '../components/layout/Layout';
import '../styles/globals.css';

function MyApp({ Component, pageProps: { session, ...pageProps } }) {
  const router = useRouter();
  const [loading, setLoading] = useState(false);

  useEffect(() => {
      const handleStart = (url) => {
        url !== router.pathname ? setLoading(true) : setLoading(false);
      };
      const handleComplete = (url) => setLoading(false);

      router.events.on("routeChangeStart", handleStart);
      router.events.on("routeChangeComplete", handleComplete);
      router.events.on("routeChangeError", handleComplete);
    }, [router]);
    
  return <SessionProvider session={session}>
    <Layout>
      {loading ? <Loading loading={loading} /> : <Component {...pageProps} />}
    </Layout>
  </SessionProvider>
  }

export default MyApp
