import About from '../components/layout/home/About'
import Biodata from '../components/layout/home/Biodata'
import Project from '../components/layout/home/Project'
import styles from '../styles/Home.module.css'

export default function Home(props) {
  return (
    <div className={styles.container}>
      <main className={styles.main}>
        <About />
        <Biodata />
        <Project />
      </main>
    </div>
  )
}


