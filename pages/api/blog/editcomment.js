import { connectToDatabase } from '../../../lib/db';
import { ObjectId } from 'mongodb';

export default async function handler(req, res){
    if(req.method === 'POST'){
        const body = req.body;

        const client = await connectToDatabase();
        const db = client.db();
      
        const blogCollection = db.collection('blog');
      
        try {
            const works = await blogCollection.updateOne(
                {_id: ObjectId(body.id), "comment.id": parseInt(body.commentId) },
                { $set: { "comment.$.comment": body.comment }}
            );

            client.close();
            res.status(201).json({message: "Edit comment success"});
        }
        catch(error) {
            client.close();
            throw new Error("Cannot edit comment")
        }
        
    }
}