import { connectToDatabase } from '../../../lib/db';

export default async function handler(req, res){
    if(req.method === 'POST'){
        const data = req.body;

        const { title, image, desc } = data;
        
        const client = await connectToDatabase();
        let db = client.db();
        db = db.collection('blog');

        const result = await db.insertOne({
            title: title,
            image: image,
            desc: desc,
            comment: [],
        });

        client.close();
        
        res.status(201).json({message: "Blog has been made!", data: result});
    }
}