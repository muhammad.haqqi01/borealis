import { connectToDatabase } from '../../../lib/db';

export default async function handler(req, res){
    if(req.method === 'GET'){
        const client = await connectToDatabase();
        const db = client.db();
      
        const blogsCollection = db.collection('blog');
      
        const blogs = await blogsCollection.find({}, { _id: 1 }).toArray();
      
        client.close();
          
        res.status(201).json({message: "Fetching success", data: blogs});
    }
}