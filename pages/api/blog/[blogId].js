import { connectToDatabase } from '../../../lib/db';
import { ObjectId } from 'mongodb';

export default async function handler(req, res){
    if(req.method === 'GET'){

        const { blogId } = req.query
        const client = await connectToDatabase();
        const db = client.db();
      
        const blogCollection = db.collection('blog');
      
        try {
            const selectedBlog = await blogCollection.findOne({_id: ObjectId(blogId)});
            client.close();
            res.status(201).json({message: "Fetching success", data: selectedBlog});
        }
        catch(error) {
            client.close();
            throw new Error("No blog found")
        }
        
    }
}