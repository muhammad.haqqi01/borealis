import { connectToDatabase } from '../../../lib/db';
import { ObjectId } from 'mongodb';

export default async function handler(req, res){
    if(req.method === 'POST'){
        const body = req.body;

        const client = await connectToDatabase();
        const db = client.db();
      
        const blogCollection = db.collection('blog');
        
        try {
            const x = await blogCollection.updateOne({_id: ObjectId(body.query)}, { $push: { comment: {
                id: body.size,
                fullname: body.fullname,
                comment: body.comment,
            }}})

            client.close();
            res.status(201).json({message: "Comment success"});
        }
        catch(error) {
            client.close();
            throw new Error("Cannot comment")
        }
        
    }
}