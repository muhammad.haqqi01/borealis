import { connectToDatabase } from '../../../lib/db';
import { ObjectId } from 'mongodb';

export default async function handler(req, res){
    if(req.method === 'POST'){
        const body = req.body;

        const client = await connectToDatabase();
        const db = client.db();
      
        const blogCollection = db.collection('blog');
      
        try {
            const works =  await blogCollection.updateOne({
                _id: ObjectId(body.id)}, 
                { $pull: { comment: { id: body.index } } },
                false,
                true
            )      

            client.close();
            res.status(201).json({message: "Delete comment success"});
        }
        catch(error) {
            client.close();
            throw new Error("Cannot delete comment")
        }
        
    }
}