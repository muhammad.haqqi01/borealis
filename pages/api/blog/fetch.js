import { connectToDatabase } from '../../../lib/db';

export default async function handler(req, res){
    if(req.method === 'GET'){
        const client = await connectToDatabase();
        let db = client.db();
        db = db.collection('blog');

        const data = await db.find().toArray();

        client.close();
        
        res.status(201).json({message: "Fetching success", data: data});
    }
}