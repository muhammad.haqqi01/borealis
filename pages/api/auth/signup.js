import { MongoClient } from 'mongodb';
import { hashPassword } from '../../../lib/auth';
import { connectToDatabase } from '../../../lib/db';

export default async function handler(req, res){
    if(req.method === 'POST'){
        const data = req.body;

        const { fullname, username, password } = data;
        
        if (!username || !password || !fullname) {
            res.status(422).json({message: "Cannot login, wrong username of password"});
            return;
        }
        
        const client = await connectToDatabase();
        let db = client.db();
        db = db.collection('users');

        const hashedPassword = await hashPassword(password);

        const result = await db.insertOne({
            fullname: fullname,
            username: username,
            password: hashedPassword,
            isAdmin: false
        });

        client.close();
        
        res.status(201).json({message: "User has been made!", data: data});
    }
}