import NextAuth from "next-auth/next";
import CredentialsProvider from "next-auth/providers/credentials";
import { verifyPassword } from "../../../lib/auth";
import { connectToDatabase } from "../../../lib/db";

export default NextAuth({
    providers: [
        CredentialsProvider({
            async authorize(credentials, req) {
                const client = await connectToDatabase();

                const usersCollection = client.db().collection('users');
                let user = await usersCollection.findOne({username: credentials.username})
                
                if(!user) {
                    client.close();
                    throw new Error ('No user found!');
                }
                
                const isValid = await verifyPassword(credentials.password, user.password)
                if (!isValid) {
                    client.close();
                    throw new Error("Wrong password");
                }

                client.close();
                const response = {
                    email: user.username,
                    name: user.fullname,
                    image: user.isAdmin
                  };
                return response;
            },
            secret:process.env.SECRET,
            session: {
                jwt: true,
            },
            callbacks: {
                async session({ session, token }) {
                  session.user = token.user;
                  return session;
                },
                async jwt({ token, user }) {
                  if (user) {
                    token.user = user;
                  }
                  return token;
                },
            },
        })
    ]
});