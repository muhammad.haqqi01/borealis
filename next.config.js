/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  images: {
    domains: [
      'unsplash.com',
      'google.com',
      'cdn.discordapp.com',
    ]
  }
}

module.exports = nextConfig
