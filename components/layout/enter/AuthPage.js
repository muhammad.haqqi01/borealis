import { motion, AnimatePresence } from "framer-motion";
import { useRouter } from "next/router";
import { useState } from "react";
import { signIn } from 'next-auth/react';
import styles from './AuthPage.module.css';
import Login from "./Login";
import Register from "./Register";

async function createUser(fullname, username, password) {
    const response = await fetch('/api/auth/signup', {
        method: 'POST',
        body: JSON.stringify({fullname, username, password}),
        headers: {
            'Content-Type': 'application/json'
        }
    });
    
    const data = await response.json()

    if(!response.ok) {
        return false;
    }
    return data;
}

const Auth = (props) => {
    const router = useRouter();
    const [wantLogin, setWantLogin] = useState(true);
    const [title, setTitle] = useState("Login Here");
    const styling = <style>{`
        form {
            margin: 32px;
        }
        input {
            background: #3C3C3C;
            margin: 12px 0px 16px 0px;
            text-indent: 4px;
            justify-self: center;
            width: 100%;
            padding: 4px 8px;
            border-radius: 4px;
        }
        input:focus {
            outline: transparent;
            border: 0px solid #F2F2F2;
        }
        p{
            margin: 8px 0px;
        }
        button {
            background: #1e40af;
            text-align: center;
            border-radius: 8px;
            padding: 8px 24px;
        }
    `}</style>

    const changePage = () => {
        if(wantLogin) setTitle("Register Here")
        else setTitle("Login Here")
        setWantLogin(!wantLogin)
    }

    async function submitHandler (props, isRegister){
        if(!isRegister) {
            try {
                const result = await signIn('credentials', {
                    redirect: false,
                    username: props.username,
                    password: props.password
                });
                if(result.error == null) router.push("/");
                else router.reload(window.location.pathname)
            } catch (error) {
                console.log("Error " + error)
            }
        }
        else {
            try {
                const isCreated = await createUser(props.fullname, props.username, props.password)
                if(isCreated) {
                    changePage();
                }
            } catch (error) {
                console.log(error + " Error smt")
            }
        }
    }

    return (
        <div className={styles.card}>
            <motion.div
                initial={{ y:10, opacity: 0 }}
                animate={{ y: 0, x: 0, opacity: 1 }}>
                <br/>
                <h2 className={styles.h2}>{title}</h2>
                {wantLogin ? <Login changePage={changePage} onChange={submitHandler} /> : <Register changePage={changePage} onChange={submitHandler} />}
                {styling}
            </motion.div>
        </div> 
    )   
}

export default Auth;