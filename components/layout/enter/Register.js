import { useRef } from "react";
import { motion } from "framer-motion";

const Register = (props) => {
    const fullnameRef = useRef();
    const usernameRef = useRef();
    const passwordRef = useRef();
  
    function submitHandler(event) {
      event.preventDefault();
  
      const enteredFullname = fullnameRef.current.value;
      const enteredUsername = usernameRef.current.value;
      const enteredPassword = passwordRef.current.value;
  
      const post = {
        fullname: enteredFullname,
        username: enteredUsername,
        password: enteredPassword,
      };
  
      props.onChange(post, true);
    }

    return (
        <form onSubmit={submitHandler}>
            <div>
                <label htmlFor='fullname'>Full Name</label><br/>
                <input type='text' required id='fullname' ref={fullnameRef} />
            </div>
            <div>
                <label htmlFor='username'>Username</label><br/>
                <input type='text' required id='username' ref={usernameRef}  />
            </div>
            <div>
                <label htmlFor='password'>Password</label><br/>
                <input type='password' required id='password' ref={passwordRef} />
            </div>
            <br/>
            <p onClick={props.changePage} className="cursor-pointer text-center">Login here if you have an account</p>
            <div className="text-center m-6">
                <motion.button whileHover={{ scale: 1.1 }} whileTap={{ scale: 0.9 }}  onSubmit={submitHandler}>Register</motion.button>
            </div>
        </form>
    )   
}

export default Register;