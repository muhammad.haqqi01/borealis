import { motion } from "framer-motion";
import {  useRef } from "react";

const Login = (props) => {
    const usernameRef = useRef();
    const passwordRef = useRef();
  
    function submitHandler(event) {
      event.preventDefault();
  
      const enteredUsername = usernameRef.current.value;
      const enteredPassword = passwordRef.current.value;
  
      const post = {
        username: enteredUsername,
        password: enteredPassword,
      };

      props.onChange(post, false);
    }

    return (
        <form onSubmit={submitHandler}>
            <div>
                <label htmlFor='username'>Username</label><br/>
                <input type='text' required id='username' ref={usernameRef} />
            </div>
            <div>
                <label htmlFor='password'>Password</label><br/>
                <input type='password' required id='password' ref={passwordRef} />
            </div>
            <br/>
            <p onClick={props.changePage} className="cursor-pointer text-center">Register here if you dont have an account</p>
            <div className="text-center m-6">
                <motion.button whileHover={{ scale: 1.1 }} whileTap={{ scale: 0.9 }}  onSubmit={submitHandler}>Login</motion.button>
            </div>
        </form>
    )   
}

export default Login;