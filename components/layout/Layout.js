import Navigation from './Navigation';

function Layout(props) {
  return (
    <div>
        <Navigation />
        <main className="px-4 lg:px-40 py-8" >
          {props.children}
        </main>
    </div>
  );
}

export default Layout;