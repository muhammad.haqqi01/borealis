import React from "react";
import { motion } from "framer-motion"

function Loading(props) {
 
  const styling = <style>{`
    .loading {
        display: flex;
        flex-direction: column;
        align-items: center;
        justify-content: center;
        height: 60vh;
    }
    .circle {
        margin: 8px 0px;
        height: 20px;
        width: 20px;
        background: white;
    }
  `}</style>
  return (
    <div className={props.loading ? "loading" : "hidden"}>
      <div className="flex flex-row justify-center gap-6">
        {styling}
        <motion.div 
            className="circle"
            animate={{
                x: [-30, 30, -30],
                rotate: [0, 0, 270, 270, 0],
                borderRadius: ["20%", "20%", "50%", "50%", "20%"]
              }}
              transition={{
                duration: 2,
                ease: "easeInOut",
                times: [0, 0.2, 0.5, 0.8, 1],
                repeat: Infinity,
                repeatDelay: 0.2,
              }}
        />
        <motion.div 
            className="circle"
            animate={{
                x: [-30, 30, -30],
                rotate: [0, 0, 270, 270, 0],
                borderRadius: ["20%", "20%", "50%", "50%", "20%"]
              }}
              transition={{
                duration: 2,
                ease: "easeInOut",
                times: [0, 0.2, 0.5, 0.8, 1],
                delay: 0.1,
                repeat: Infinity,
                repeatDelay: 0.2,
              }}
        />
        <motion.div 
            className="circle"
            animate={{
                x: [-30, 30, -30],
                rotate: [0, 0, 270, 270, 0],
                borderRadius: ["20%", "20%", "50%", "50%", "20%"]
              }}
              transition={{
                duration: 2,
                ease: "easeInOut",
                times: [0, 0.2, 0.5, 0.8, 1],
                repeat: Infinity,
                delay: 0.2,
                repeatDelay: 0.2,
              }}
        />
      </div>
    </div>
  );
}

export default Loading;