import Image from "next/image";
import { motion } from "framer-motion"
import BorderedFrame from "../../UI/BorderedFrame";
import DarkBackground from "../../UI/DarkBackground"

const About = () => (
    <motion.section className='flex flex-col md:flex-row gap-8 mb-16'
          initial={{ y: 30, opacity: 0 }}
          animate={{ y: 0, opacity: 1 }}
          transition={{ delay: `0.05` }}>
        <BorderedFrame className='relative'>
            <Image priority={true} className="rounded-[8px]" src='/static/images/mypic.jpg' alt="me" layout="fill" objectFit="cover"/>
        </BorderedFrame>
        <DarkBackground className='relative'>
            <h2 className="mb-4">Tentang Diriku</h2>
            <p>
              Halo semua, namaku Muhammad Haqqi Al Farizi biasa dipanggil Eki, Haqqi, dan banyak panggilan lainnya hehe. Aku adalah
              salah satu anak Kalimantan yang kuliah di Fasilkom, suatu populasi langka nih. Oh iya, aku dari jurusan Ilmu Komputer 2020 Kerjaanku dari semester 2 itu selalu 
              ngewebdev di kuliah maupun di organisasi. Biarpun pengalaman webdevku itu di PTI tahun lalu aja, tetapi aku selalu
              menggunakan kesempatan dengan baik looh. Di bawah adaatuh web yang aku ikut proyekannya selama aku jadi webdev.
              <br/>
            </p>
        </DarkBackground>
    </motion.section>
)

export default About;