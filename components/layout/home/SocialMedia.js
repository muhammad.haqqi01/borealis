import Icon from "../../UI/Icon";

const SocialMedia = () => (
    <>
        <h3 className="text-lg font-semibold">Social Media dan Link</h3>
        <section className='flex flex-row flex-wrap gap-4' style={{width: "100%"}}>
            <Icon name="facebook.png" url="https://web.facebook.com/bukanmuhammad.kik/" />
            <Icon name="linkedin.png" url="https://www.linkedin.com/in/muhammad-haqqi-a-117a18190/" />
            <Icon name="instagram.png" url="https://www.instagram.com/bukan_bukanbukanmuhammad_kik/" />
            <Icon name="gitlab.png" url="https://gitlab.com/muhammad.haqqi01" />
            <Icon name="github.png" url="https://github.com/muhammadhaqqi" />
        </section>
    </>
)

export default SocialMedia;