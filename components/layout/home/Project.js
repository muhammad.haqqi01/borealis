import Image from "next/image";
import DarkBackground from "../../UI/DarkBackground";
import PictureWithDesc from "../../UI/PictureWithDesc";
import { motion } from "framer-motion";

const Project = () => {
    const picture = [
        {
            name: 'best_staff',
            alt: 'best-staff',
            desc: 'BEM Best Staff'
        },
        {
            name: 'lpj_online',
            alt: 'lpj-online',
            desc: 'New LPJ Online'
        },
        {
            name: 'ews_site',
            alt: 'ews-site',
            desc: 'Experience Worth Sharing'
        },
        {
            name: 'project_channel',
            alt: 'project-channel',
            desc: 'Project Channel'
        }
    ]

    return (
        <motion.section 
            className='flex-row gap-8 mb-8' 
            style={{width: "100%"}}
            initial={{ y: 30, opacity: 0 }}
            animate={{ y: 0, opacity: 1 }}
            transition={{ delay: `0.15` }}>
            <h2 className="mb-4">Partisipasi Proyek Sebelumnya</h2>
            <DarkBackground>
                <div className="flex flex-wrap gap-x-16 gap-y-8 justify-center">
                    {picture.map((i) => 
                        <PictureWithDesc key={i.name}>
                            <Image width={320} height={204} alt={i.alt} src={`/static/images/${i.name}.jpg`} className="rounded-lg" objectFit="cover" />
                            <p className="font-medium" style={{textAlign: "center"}}>{i.desc}</p>
                        </PictureWithDesc>
                    )}
                </div>
            </DarkBackground>
        </motion.section>
)}

export default Project;