import DarkBackground from "../../UI/DarkBackground";
import SocialMedia from "./SocialMedia";
import { motion } from "framer-motion";

const Biodata = () => {

    const styling = <style>{`
        h6 {
            margin-top: 0px;
            margin-bottom: 16px;
            font-weight: bold;
        }
    `}</style>

    return (
        <motion.section 
          className='flex-row gap-8 mb-16' 
          style={{width: "100%"}}
          initial={{ y: 30, opacity: 0 }}
          animate={{ y: 0, opacity: 1 }}
          transition={{ delay: `0.2` }}>
            {styling}
            <h2 className="mb-4">Biodata</h2>
            <DarkBackground>
                <div className="flex flex-col md:flex-row mb-8">
                    <div className="flex-col md:w-1/2">
                        <h6>Nama</h6><p> Muhammad Haqqi Al Farizi</p><br/>
                        <h6>Tempat, Tanggal Lahir</h6><p> Bontang, 26 Desember 2001</p><br/>
                        <h6>Alamat</h6><p>Jl.Tanjung No.79 Komplek PT Badak, Bontang</p><br/>
                        <h6>Email</h6><p>csc.muhammadhaqqi@gmail.com</p><br/>
                        <h6>Nomor HP</h6> <p>081215521312</p>
                    </div>
                    <div className="flex-col w-1/2">
                        <br className="md:hidden"/>
                        <h6>Kampus</h6><p>Universitas Indonesia</p><br/>
                        <h6>Jurusan</h6><p>Ilmu Komputer 2020</p><br/>
                        <h6>Pengalaman Organisasi dan Panitia</h6>
                        <p>
                        - Deputi PTI BEM Fasilkom UI 2022 <br/> 
                        - Staff PTI BEM Fasilkom UI 2021 <br/> 
                        - Staff Perlengkapan BETIS Fasilkom UI 2021
                        </p><br/>
                        <h6>Tech Stacks</h6><p>Next.js(baru), React, Django, Java, Python, Figma</p><br/>
                    </div>
                </div>
                <SocialMedia />
            </DarkBackground>
        </motion.section>
)}

export default Biodata;