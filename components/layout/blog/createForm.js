import { motion } from "framer-motion";
import { useRef } from "react";

const CreateForm = (props) => {
    const titleRef = useRef();
    const imageRef = useRef();
    const descRef = useRef();
  
    function submitHandler(event) {
      event.preventDefault();
  
      const enteredtitle = titleRef.current.value;
      const enteredImage = imageRef.current.value;
      const enteredDesc = descRef.current.value;
  
      const post = {
        title: enteredtitle,
        image: enteredImage,
        desc: enteredDesc,
      };

      props.onSubmit(post);
    }

    return (
        <form onSubmit={submitHandler}>
            <div>
                <label htmlFor='title'>Title</label><br/>
                <input type='text' required id='title' ref={titleRef} />
            </div>
            <div>
                <label htmlFor='image'>Heading Image</label><br/>
                <input type='text' placeholder="Taruh link disini" required id='image' ref={imageRef} />
            </div>
            <div>
                <label htmlFor='text'>Text</label><br/>
                <textarea type='textarea' required id='text' ref={descRef} />
            </div>
            <br/>
            <div className="text-center m-6">
                <motion.button whileHover={{ scale: 1.1 }} whileTap={{ scale: 0.9 }} onSubmit={submitHandler}>Create</motion.button>
            </div>
        </form>
    )   
}

export default CreateForm;