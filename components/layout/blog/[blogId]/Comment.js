import { useRef } from "react";
import { useSession } from "next-auth/react";
import { useState, useEffect } from 'react';
import { motion } from 'framer-motion';
 
const Comment = (props) => {
    const [isEdit, setIsEdit] = useState(Array(props.comment.length).fill(false));
    const { data: session, status } = useSession();
    const commentRef = useRef();
    const editCommentRef = useRef();

    const CommentBar = (i) => (<div className="commentCard">
        <div className="flex flex-row gap-x-4 place-items-center flex-wrap">
            <h5 className="font-semibold">{i.i.fullname}</h5>
            {status === "authenticated" && i.i.fullname === session.user.name  ? 
            <>
                <span className="text-sm cursor-pointer" onClick={() => editComment(i.index)}>edit</span>
                <span className="text-sm cursor-pointer" onClick={() => deleteComment(i.i.id)}>delete</span> 
            </>
            : null}
        </div>
        <p>{i.i.comment}</p>
    </div>)

    const EditComment = (props) => (<form onSubmit={editHandler}>
        <input type="hidden" value={props.index} id='index' hidden />
        <textarea className="commentCard" placeholder="Enter comment..." ini required id='text' ref={editCommentRef}>{props.i.comment}</textarea>
        <motion.button whileHover={{ scale: 1.1 }} whileTap={{ scale: 0.9 }}  onSubmit={() => editHandler} className="rounded-lg bg-blue-800 px-6 py-2 font-semibold outline-black align-end">Send</motion.button>
    </form>)

    async function submitHandler(event) {
        event.preventDefault();
  
        const fullname = session.user.name;
        const enteredComment = commentRef.current.value;
        let size = 0;

        if(enteredComment == null) return;

        if(props.comment.length > 0) {
            const ids = props.comment.map(object => {
                return object.id;
            });// 👉️ [1, 7, 3, 14]
              
            size = Math.max(...ids)+1;
        }

        const post = {
            id: size,
            fullname: fullname,
            comment: enteredComment,
        };
        
        await props.onSubmit(post);
        let newEditState = [...isEdit];
        newEditState.push(false)
        setIsEdit(newEditState);
    }

    async function editHandler(event) {
        event.preventDefault();
  
        const id = index.value;
        const fullname = session.user.name;
        const enteredComment = editCommentRef.current.value;

        if(enteredComment == undefined) {
            let newEditState = [...isEdit];
            newEditState[index.value] = false;
            setIsEdit(newEditState);
        }

        const post = {
            id: id,
            fullname: fullname,
            comment: enteredComment,
        };
        
        await props.onEdit(post);
        let newEditState = [...isEdit];
        newEditState[index.value] = false;
        setIsEdit(newEditState);
    }

    const editComment = (index) => {
        let newEditState = [...isEdit];
        newEditState[index] = true;
        setIsEdit(newEditState);
    }

    async function deleteComment(index) {
        await props.onDelete(index);
        let newEditState = [...isEdit];
        let finded = props.comment.findIndex((obj => obj.id == index))
        newEditState.splice(finded, 1);
        setIsEdit(newEditState);
    }

    const styling = <style>{`
        .commentCard, textarea {
            width: 100%;
            background: #EAEAEA;
            color: black;
            padding: 12px 16px;
            border-radius: 8px;
            margin: 16px 0px;
        }
        input:focus, textarea:focus {
            outline: transparent;
            border: 0px solid #F2F2F2;
        }
    `}</style>

    console.log(status)

    return (
        <>
            {props.comment.length > 0 ? <h2 className='font-bold text-2xl'>People&apos;s Comment</h2> : null}
            {props.comment.map((i, index) => (
                !isEdit[index] ? <CommentBar i={i} index={index}/> : <EditComment i={i} index={index} />
            ))}
            <br/>
            {status === "authenticated" ?
            <>
                <h2 className='font-bold text-2xl'>Comment here</h2>
                <form onSubmit={submitHandler}>
                    <textarea className="commentCard" placeholder="Enter comment..." required id='text' ref={commentRef} />
                    <motion.button whileHover={{ scale: 1.1 }} whileTap={{ scale: 0.9 }} onSubmit={submitHandler} className="rounded-lg bg-blue-800 px-6 py-2 font-semibold outline-black align-end">Send</motion.button>
                </form>
            </> : null}
            {styling}
        </>
    )
}

export default Comment;