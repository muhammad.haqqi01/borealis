import Blog from "../../../../pages/blog";
import Image from "next/image";

const BlogSection = (props) => {
    return (
        <>
            <h2 className='font-bold text-3xl'>{props.title}</h2>
            <br/>
            <div className='relative' style={{height:"400px"}}>
                <Image priority={true} src={`/api/imagefetcher?url=${encodeURIComponent(props.image)}`} alt="me"  layout='fill' objectFit={'cover'}/>
            </div>
            <br/>
            {console.log(props.desc)}
            {props.desc.map((i, index) => (
                index % 2 == 0 ? <><p>{i}</p><br/></> : null
            ))}
        </>
    )
}

export default BlogSection;