import Image from 'next/image';
import { useState, useEffect } from 'react';

const styling = <style>{`
.bigcard {
  position: relative;
  width: 100%;
  height: 500px;
  border-radius: 8px;
}
.bigcardTitle{
  margin: 0px 12px 0px 24px;
  color: black;
  font-size: 36px;
  font-weight: bold;
  position: relative;
  bottom: 100px;
  z-index: 1;
  -webkit-user-select: none; /* Safari */        
  -moz-user-select: none; /* Firefox */
  -ms-user-select: none; /* IE10+/Edge */
  user-select: none; /* Standard */
}
.bigcardDesc {
  color: black;
  margin: 4px 12px 4px 24px;
  font-size: 24px;
  position: relative;
  font-weight: medium;
  bottom: 110px;
  z-index: 1;
  -webkit-user-select: none; /* Safari */        
  -moz-user-select: none; /* Firefox */
  -ms-user-select: none; /* IE10+/Edge */
  user-select: none; /* Standard */
}
`}</style>

function useWindowSize() {
    // Initialize state with undefined width/height so server and client renders match
    // Learn more here: https://joshwcomeau.com/react/the-perils-of-rehydration/
    const [windowSize, setWindowSize] = useState({
      width: undefined,
      height: undefined,
    });
  
    useEffect(() => {
      // only execute all the code below in client side
      if (typeof window !== 'undefined') {
        // Handler to call on window resize
        function handleResize() {
          // Set window width/height to state
          setWindowSize({
            width: window.innerWidth,
            height: window.innerHeight,
          });
        }
      
        // Add event listener
        window.addEventListener("resize", handleResize);
       
        // Call handler right away so state gets updated with initial window size
        handleResize();
      
        // Remove event listener on cleanup
        return () => window.removeEventListener("resize", handleResize);
      }
    }, []); // Empty array ensures that effect is only run on mount
    return windowSize;
  }

function BigCard(props) {
    let windowSize = useWindowSize();
    console.log(windowSize)

    let desc = props.desc

    if(props.desc.length > 70) {
        desc = desc.substring(0, windowSize.width/19) + "...";
    }

    return (
        <div className="bigcard">
            <div className='bg-white absolute rounded-[8px]' style={{width: "100%", height: "100%", zIndex:'1', opacity:"30%"}} ></div>
            <div style={{width: "100%", height: "100%", position:"relative"}} >
                <Image priority={true} className="rounded-[8px]" src={`/api/imagefetcher?url=${encodeURIComponent(props.img)}`} layout={'fill'} objectFit={'cover'} />
            </div>
            <h4 onselectstart="return false" className="bigcardTitle">{props.title}</h4>
            <p onselectstart="return false" className="bigcardDesc">{desc}</p>
            {styling}
        </div>
    );
}

export default BigCard;
