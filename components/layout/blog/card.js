import Image from 'next/image';
import { useState } from 'react';

const styling = <style>{`
.card {
  position: relative;
  width: 100%;
  min-width: 300px;
  max-width: 500px;
  height: 320px;
  background: #EAEAEA;
  border-radius: 8px;
}
.cardTitle{
  margin: 12px 12px 0px 12px;
  color: black;
  font-size: 16px;
  font-weight: bold;
}
.cardDesc {
  color: black;
  margin: 8px 12px 8px 12px;
}
`}</style>

function Card(props) {
  let desc = props.desc
  console.log(props.img)

  if(props.desc.length > 32) {
    desc = desc.substring(0, 32) + "..."
  }

  return (
      <div className="card">
        <div style={{width: "100%", height: "73%", position:"relative"}}>
          <Image priority={true} className="rounded-t-[8px]" src={`/api/imagefetcher?url=${encodeURIComponent(props.img)}`} layout={'fill'} objectFit={'cover'} />
        </div>
        <h4 className="cardTitle">{props.title}</h4>
        <p className="cardDesc">{desc}</p>
        {styling}
      </div>
  );
}

export default Card;
