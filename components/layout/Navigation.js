import Link from 'next/link';
import { useSession, signOut, getSession } from 'next-auth/react';
import { motion } from 'framer-motion';

function Navigation(props) {
  const { data: session, status } = useSession();

  function logoutHandler() {
    signOut();
  }

  return (
    <header>
      <nav className="flex sm:justify-end space-x-12 px-16 py-8 place-items-center font-semibold">
        <Link href='/'>
            <motion.p whileHover={{ scale: 1.1 }} whileTap={{ scale: 0.9 }} className="cursor-pointer">Home</motion.p>
        </Link>
        <Link href='/blog'>
            <motion.p whileHover={{ scale: 1.1 }} whileTap={{ scale: 0.9 }} className="cursor-pointer">Blog</motion.p>
        </Link>
        {status == "authenticated" ? 
          <motion.button whileHover={{ scale: 1.1 }} whileTap={{ scale: 0.9 }} onClick={logoutHandler} className="rounded-lg bg-red-800 px-6 py-2 font-semibold outline-black">Logout</motion.button>
        : <Link href='/enter'>
          <motion.button whileHover={{ scale: 1.1 }} whileTap={{ scale: 0.9 }} className="rounded-lg bg-blue-800 px-6 py-2 font-semibold outline-black">Login</motion.button>
        </Link>}
      </nav>
    
    </header>
  );
}

export default Navigation;

export async function getServerSideProps(context) {
  return {
    props: {
      session: await getSession(context),
    },
  }
}