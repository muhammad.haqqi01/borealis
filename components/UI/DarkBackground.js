import Head from 'next/head'
import Image from 'next/image'
import styles from './DarkBackground.module.css'

export default function DarkBackground(props) {
  return (
    <section className={styles.main}>
        {props.children}
    </section>
  )
}
