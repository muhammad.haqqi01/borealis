import Head from 'next/head'
import Image from 'next/image'

export default function PictureWithDesc(props) {
  return (
    <div>
        {props.children}
    </div>
  )
}
