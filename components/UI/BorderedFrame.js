import Head from 'next/head'
import Image from 'next/image'
import styles from './BorderedFrame.module.css'

export default function BorderedFrame(props) {
  return (
    <div className={`xl:mb-0  ${styles.main}`}>
        {props.children}
    </div>
  )
}
