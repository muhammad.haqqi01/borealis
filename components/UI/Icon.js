import Image from 'next/image'
import { motion } from 'framer-motion'

export default function Icon(props) {
    return (
        <a target="_blank" rel="noreferrer" href={props.url}>
            <motion.div whileHover={{ scale: 1.1 }} whileTap={{ scale: 0.9 }}  className='relative bg-white p-2 rounded-lg cursor-pointer' style={{width: "50px", height: "50px"}}>
                <Image priority={true} src={`/static/icon/${props.name}`} width={80} height={80} className='bg-white' objectFit='cover' />
            </motion.div>
        </a>
    )
}
